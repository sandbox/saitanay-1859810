-- SUMMARY --

    The Block Inject Filter Module Provides an Input Filter that you can add 
    to any of your Input Formats. Once enabled, the filter allows you to
    inject (insert) any Custom Block into the content at the end of 
    a predefined number of paragraph.


-- REQUIREMENTS --

Filter Module, which is available in Drupal Core.


-- INSTALLATION --

* Download and Install the Module

-- CONFIGURATION --

* Edit any of your Input Format (say Full HTML)
* Enable "Block Inject" filter
* In the configuration form of the filter, provide below details:
    1) The Custom Block which has to be inserted
    2) The tag after which the Block has to be injected
        You can choose to inject the block after the close of Nth <p> tag, or
        <div> tag or a New Line
    3) The value of N (The number of the paragraph after which you 
        wish to insert)


-- TROUBLESHOOTING --

* If you have other filters enabled along with Block Inject filter, observe how 
    are changing the output. It is recommended to place Block Inject Fiter on
    top of all other filters
* Beware than though your actual content does not have any <p> tag, the could be
    inserted by Drupal, if you have other filters like "Convert line breaks 
    into HTML" enabled.
* Try changing the value N and the tag, in the configuration of the filter to
    achieve your expected results
* ENSURE YOUR INJECTED BLOCK HAS A DIFFERENT INPUT FORMAT, ELSE THE DISPLAY 
    COULD GO INTO A LOOP WITH THE MODULE TRYING TO INJECT THE BLOCK AGAIN 
    INTO THE INJECTED BLOCK!


-- CONTACT --

Current maintainers:
* Tanay Sai ( http://drupal.org/user/220136 )
